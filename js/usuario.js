if(JSON.parse(localStorage.getItem('user')) == ''){
  window.location.href = 'index.html';
}

class Socio {
  constructor(nombre, apellido, dni, membresia, mail, contraseña, estado) {
    this.nombre = nombre;
    this.apellido = apellido;
    this.dni = dni;
    this.membresia = membresia;
    this.mail = mail;
    this.contraseña = contraseña;
    this.estado = estado;
    this.inscripciones = [];
  }
}
class Membresia {
  constructor(membresia, costo) {
    this.membresia = membresia;
    this.costo = costo;
  }
}

class Actividades {
  constructor(clase, hora, dia, capacidad) {
    this.clase = clase;
    this.hora = hora;
    this.dia = dia;
    this.capacidad = capacidad;
  }
}

class Sociosmembresiasactividad {
  constructor(
    nombre,
    apellido,
    plan,
    n_clases,
    mail,
    clases,
    hs,
    dia,
    contador
  ) {
    this.nombre = nombre;
    this.apellido = apellido;
    this.plan = plan;
    this.n_clases = n_clases;
    this.mail = mail;
    this.clases = clases;
    this.hs = hs;
    this.dia = dia;
    this.contador = contador;
  }
}



class Clases {
  constructor(nombre, horarios, imagen) {
    this.nombre = nombre;
    this.horarios = horarios;
    this.imagen = imagen;
  }
}
/* 
  let clases = JSON.parse(localStorage.getItem('clases')) || [];
  mostrarClases();
  
  function guardarImagen(){
    let nombre_clase = document.getElementById('nombre_clase').value;
    let horarios = document.getElementById('horarios').value;
    let nombre_imagen = document.getElementById('nombre_imagen');
    let direccion_imagen = 'img/' + nombre_imagen.files[0].name;
  
    console.log(nombre_clase);
    console.log(horarios);
    console.log(direccion_imagen);
  
    let clase = new Clases(nombre_clase, horarios, direccion_imagen);
    clases.push(clase);
    localStorage.setItem('clases',JSON.stringify(clases));
    mostrarClases();
    limpiar();
  }
  
  function mostrarClases(){
    let tarjeta = document.getElementById('tarjeta');
    tarjeta.innerHTML = '';
    let contenido = '';
    for (let i = 0; i < clases.length; i++) {
      contenido += '<a href="#" class="contenedor_tarjeta bordes_tarjetas rounded ml-5">';
      contenido += '<figure>';
      contenido += '<img src="'+clases[i].imagen+'" alt="" class="frontal">';
      contenido += '<figcaption class="trasera">';
      contenido += '<h2>'+clases[i].nombre+'</h2><hr>';
      contenido += '<p><h5>Horarios: '+clases[i].horarios+'</h5></p>';
      contenido += '<button type="button" class="btn btn-danger">Reservar</button></figcaption></figure></a>';
    }
    tarjeta.innerHTML = contenido
  }
  
  function limpiar(){
    document.getElementById('nombre_clase').value = '';
    document.getElementById('horarios').value = '';
    document.getElementById('nombre_imagen').value = '';
    
  }

  let clases = JSON.parse(localStorage.getItem('actividad')) || [];
  let user = JSON.parse(localStorage.getItem('user'));
  let arraysocios=JSON.parse(localStorage.getItem('socios')) || [];


document.getElementById('parrafo').innerHTML = "Las clases publicadas corresponden al dia " + obtenerDia();

function obtenerDia(){
    let dia = new Date();
    let nombreDias = ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'];
    return(nombreDias[dia.getDay() + 1]);
}


function filtrarClases(){
    let clasesFiltradas = clases.filter( nombre => nombre.dia == obtenerDia());
    return clasesFiltradas;
}

function mostrarClases(){
    let fila = document.getElementById('fila');
    contenido = '';
    fila.innerHTML = '';
    filtrarClases().map(actividad => {
        contenido += '<div class="col-12 col-md-6 col-lg-4 contenedor_tarjeta m-0 mt-2 mb-3"><figure id="tarjeta">';
        contenido += '<img src="'+actividad.direccion+'" alt="" class="frontal  border border-danger"><figcaption class="trasera">';
        contenido += '<h2>'+actividad.clase+'</h2><hr>';
        contenido += '<h5> Horario: '+actividad.hora+'</h5><br>';
        contenido += '<h5> Cupos: '+actividad.ocupados+'/'+actividad.capacidad+'</h5><br><button class="btn btn-danger" onclick="reservarTurno(\''+actividad.clase+'\')">Inscribirme</button></figcaption></figure></div>';
        
    })
    fila.innerHTML = contenido;
}

function reservarTurno(nombre){
    console.log(nombre);
    for (let i = 0; i < filtrarClases().length; i++) {
        if(nombre == filtrarClases()[i].clase){
            clases.map(nombreClase => {if(nombreClase.clase == nombre){
                if(nombreClase.capacidad > nombreClase.ocupados){
                    filtrarClases()[i].ocupados = Number(filtrarClases()[i].ocupados) + 1;
                    nombreClase.ocupados = Number(filtrarClases()[i].ocupados);
                    console.log(nombreClase.ocupados);
                    localStorage.setItem('actividad', JSON.stringify(clases));
                    alert('Inscripción realizada correctamente');
                }
                else{
                    alert('No hay lugares disponibles');
                }
            }})
            mostrarClases();
        }
        
    }
}

function reservarTurno(nombre){
    console.log(nombre);
    for (let i = 0; i < filtrarClases().length; i++) {
        if(nombre == filtrarClases()[i].clase){
            clases.map(nombreClase => {if(nombreClase.clase == nombre){
                if(nombreClase.capacidad > nombreClase.ocupados){
                    for (var k = 0; k < arraysocios.length; k++) {
                        if(arraysocios[k].mail == user){
                            for(let j = 0; j < arraysocios[k].inscripciones.length; j++){
                                if(nombre == arraysocios[k].inscripciones[j]){
                                    alert('Ya se encuentra inscripto a esta clase');
                                    return;
                                }
                                  
                            }
                                                               
                            arraysocios[k].inscripciones.push(nombre);
                            localStorage.setItem('socios', JSON.stringify(arraysocios));
                            filtrarClases()[i].ocupados = Number(filtrarClases()[i].ocupados) + 1;
                            nombreClase.ocupados = Number(filtrarClases()[i].ocupados);
                            console.log(nombreClase.ocupados);
                            localStorage.setItem('actividad', JSON.stringify(clases));
                            alert('Inscripción realizada correctamente');
                                
                            

                        }
                        
                    }
                }
                else{
                    alert('No hay lugares disponibles');
                }
            }})
            mostrarClases();
        }
        
    }
}

mostrarClases();*/

//historial();
let contador;

let arraysocios = JSON.parse(localStorage.getItem("socios")) || [];
let arraymembresias = JSON.parse(localStorage.getItem("membresias")) || [];
let arrayactividades = JSON.parse(localStorage.getItem("actividad")) || [];

let user = JSON.parse(localStorage.getItem("mail"));
alert("Ingreso a la pagina el mail de =" + user);
let indice_socio = arraysocios.findIndex(socios=> socios.mail == user);
console.log("indice_socio"+indice_socio);
console.log(arraymembresias);
indice_membresia=arraymembresias.findIndex(membresias =>(membresias.membresia==arraysocios[indice_socio].membresia));
console.log(indice_membresia);
let cant_clase = arraymembresias[indice_membresia].cant_clases;

let actxsocio = JSON.parse(localStorage.getItem("actxsocio")) || [];
let arrayactxsocio = JSON.parse(localStorage.getItem("actxsocio")) || [];
let arrayactxsociofilter = actxsocio.filter(
  (actxsocio) => actxsocio.mail == user
);

crearoption();
// crea lista de actividades del dia;
historial();
function crearoption() {
  let fecha = document.getElementById("fechahoy");
  moment.locale("es");
  let manana = moment().add(1, "d").format("dddd").toUpperCase();
  fecha.innerHTML = "Clases disponibles para mañana: " + manana;
  let html = "";
  let ul = document.getElementById("optionreserva");
  let arrayxdia=arrayactividades.filter(actxsocio=>actxsocio.dia.toUpperCase()==manana);
  console.log(arrayxdia);



  arrayxdia.map((actividad) => {

html += `<tr className="m-3"><th scope="row"> 
<button class="btn btn-outline-danger m-1 mb-3"id="${actividad.clase}${actividad.hora}" onclick="inscripcion('${actividad.clase}','${actividad.hora}','${actividad.dia}')">Reservar</button>

<td>${actividad.clase}  ${actividad.hora}  </td>
</tr><br>`;

  });
  ul.innerHTML = html;
}

function inscripcion(clase,hs,dia) {
if(arraysocios[indice_socio].estado=="Activo") {
document.getElementById(clase+hs).disabled=true;
 
   
 console.log(arrayactxsociofilter);
  let n_clase = cant_clase;
  moment.locale("es");
  let manana = moment().add(1, "d").format("dddd").toUpperCase();
  if (arrayactxsocio == "" || arrayactxsociofilter == "") {
    contador = parseInt(cant_clase) - 1;
  } else {
    contador =arrayactxsociofilter[arrayactxsociofilter.length - 1].contador - 1;
  }
  let socio_act_obj = new Sociosmembresiasactividad(
    arraysocios[indice_socio].nombre,
    arraysocios[indice_socio].apellido,
    arraysocios[indice_socio].membresia,
    cant_clase,
    user,
    clase,
    hs,
    dia,
    contador
  );
  arrayactxsocio.push(socio_act_obj);
  arrayactxsociofilter = arrayactxsocio.filter(
    (actxsocio) => actxsocio.mail == user);
    actxsocio=arrayactxsociofilter ;
 historial();
  localStorage.setItem("actxsocio", JSON.stringify(arrayactxsocio));
 //historial(arrayactxsociofilter);
  console.log(arrayactxsocio);
  crearcards(
    arraysocios[indice_socio].nombre,
    contador,
    arraysocios[indice_socio].membresia,
    arraymembresias[indice_membresia].cant_clases,
    clase,
    hs
  );
  console.log("contador" + contador);
  if (contador < 0 || manana == "DOMINGO") {
    if (manana == "DOMINGO") {
      alert(
        "A partir de mañana podras reservar tus" + n_clases + "de esta semana"
      );
      arrayactxsocio = arrayactxsocio.filter(
        (actxsocio) => actxsocio.mail !== user
      );
      localStorage.setItem("actxsocio", JSON.stringify(arrayactxsocio));
    } else {
      alert(
        "Usted ya reservo todas sus actividades permitidas por esta semana"
      );
      let socio_act_obj = new Sociosmembresiasactividad(
        arraysocios[indice_socio].nombre,
        arraysocios[indice_socio].apellido,
        arraysocios[indice_socio].membresia,
        cant_clase,
        user,
        clase,
        hs,
        dia,
        -1
      );
      arrayactxsocio.push(socio_act_obj);
      localStorage.setItem("actxsocio", JSON.stringify(arrayactxsocio));
      //actxsocio=arrayactxsocio.filter(actxsocio=> actxsocio.mail==user);
      //console.log(actxsocio);
     // historial();
    }
  }
}
else
{
alert("Usted no Podra reservar porque a sido suspendido.Comuniquese con su vendedor.");
}
}
function crearcards(nombre, contador, membresia, nclase, clase, hora) {
  let html;
  let card = document.getElementById("carnet");
  html = `<div class="border border-light p-3 text-light d-flex ">
    <div class="">
    <h5 class="text-center">Hola ${nombre} </h5>
    <h5 class="text-center">Membresia : ${membresia} </h5>
    <p  class=" m-2">Tu plan te habilita ${nclase} clases x semana. </p>
    <p  class=" m-2 mt-2">Te quedan ${contador} para reservar esta semana. </p>
    <p class="text-center">Clase de <h5> ${clase}  Reservada.</h5></p>
   </div>
   </div>`;
  card.innerHTML = html;
}

function historial(){
  let direccion = document.getElementById("historial");
  let arrayxsocio = actxsocio.filter(actxsocio => actxsocio.mail == user && actxsocio.contador>=0);
  console.log(arrayxsocio);
  // contador=arrayxsocio[arrayxsocio.lenght-1].contador;
  let html = "";
  arrayxsocio.map((actxsocio) => {
    html += `<tr>
        <td>${actxsocio.clases}</td>
        <td>${actxsocio.hs}</td>
        <td>${actxsocio.dia}</td>
        <td>${actxsocio.contador}</td>
        </tr>`;
  });
  direccion.innerHTML = html;
}

function sesionUsuario(){
  if(user != ''){
      let barra_nav = document.getElementById('barra_nav');
      let contenido = '';
      barra_nav.innerHTML = '';
      contenido += '<a class="nav-link active" href="index.html">Home</a><a class="nav-link" href="servicios.html">Servicios</a><a class="nav-link" href="contacto.html">Contacto.</a>';
      contenido += '<a class="nav-link dropdown-toggle d-flex flex-nowrap" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-user ml-2"></i></a>';
      contenido += '<div class="dropdown-menu p-1" aria-labelledby="navbarDropdown"><h6>'+user+'</h6>';
      if(user == 'admin@admin.com'){
          contenido += '<a class="dropdown-item" href="administrador.html">Gestion</a>';
      }else{
          contenido += '<a class="dropdown-item" href="usuario.html">Clases</a>'
      }
      contenido+= '<a class="dropdown-item" onClick="cerrarSesion()">Cerrar sesión</a></div>';
      barra_nav.innerHTML = contenido;
  }else{
      let barra_nav = document.getElementById('barra_nav');
      let contenido = '';
      barra_nav.innerHTML = '';
      contenido += '<a class="nav-link active" href="index.html">Home</a><a class="nav-link" href="servicios.html">Servicios</a><a class="nav-link" href="contacto.html">Contacto.</a>        <button class="btn btn-outline-danger ml-5" id="btnModal">Iniciar Sesión</button>';
      barra_nav.innerHTML = contenido;
  }
}

sesionUsuario();

function cerrarSesion(){
  user = '';
  localStorage.setItem('user',JSON.stringify(user));
  window.location.href = 'index.html';
}